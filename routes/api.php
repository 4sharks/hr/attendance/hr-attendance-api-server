<?php

use App\Http\Controllers\AttendanceRecordsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use function Laravel\Prompts\info;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware(['cors'])->group(function () {
    Route::post('/hr-attendance', [AttendanceRecordsController::class,'store'] );
    Route::post('/hr-attendance/get-by-tenant', [AttendanceRecordsController::class,'getByTenant'] );
    Route::post('/hr-attendance/get-monthy-report-by-tenant', [AttendanceRecordsController::class,'getMonthyReportByTenant'] );
});

// Route::get('/hr-attendance', function (Request $request){
//     info(json_encode($request->all()));
//     return $request->all();
// });
