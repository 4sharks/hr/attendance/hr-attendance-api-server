<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AttendanceRecordsModel extends Model
{
    use HasFactory;

    protected $table = 'hr_attendance_records';

    protected $guarded = [];

}
