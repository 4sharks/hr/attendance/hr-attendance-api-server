<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\LocationService;

class EmpLocationController extends Controller
{
    public function check_in_location($lat1,$lon1,$lat2,$lon2,$unit="K"){

        if(LocationService::distance($lat1,$lon1,$lat2,$lon2,$unit="K") < 1 ){
            return response()->json([
                "status" => 1,
                "message" => "true"
            ],200);
        }

        return response()->json([
            "status" => 0,
            "message" => "false"
        ],200);
    }



}
