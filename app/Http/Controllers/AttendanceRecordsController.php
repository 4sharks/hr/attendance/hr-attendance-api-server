<?php

namespace App\Http\Controllers;

use App\Models\AttendanceRecordsModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class AttendanceRecordsController extends Controller
{
    public function store(Request $request)
    {
        $emp_name_ar = null;
        $emp_name_en = null;
        $emp = DB::table('hr_employees')->where([
            'tenant_id' => $request->input('tenant_id'),
            'company_id' => $request->input('company_id'),
            'emp_no' => $request->input('emp_no')
        ])->first();
        if($emp){
            $emp_name_ar = $emp->name_ar;
            $emp_name_en = $emp->name_en;
        }
        $result = AttendanceRecordsModel::create([
            'emp_no' => $request->input('emp_no'),
            'emp_name_ar' => $emp_name_ar,
            'emp_name_en' => $emp_name_en,
            'type' => $request->input('type'),
            'location_id' => $request->input('location_id'),
            'location_name' => $request->input('location_name'),
            'lat' => $request->input('lat'),
            'lng' => $request->input('lng'),
            'provider_type' => $request->input('provider_type'),
            'provider_device' => $request->input('provider_device'),
            'attendance_date' => $request->input('attendance_date'),
            'tenant_id' => $request->input('tenant_id'),
            'company_id' => $request->input('company_id'),
            'branch_id' => $request->input('branch_id'),
        ]);

        if($result){
            return response([
                'status' => 1,
                'message' => 'success',
                'data' => $result
            ]);
        }
        return response([
            'status' => 0,
            'message' => 'Error processing'
        ]);
    }



    public function getByTenant(Request $request){
        $limit = 25;
        $start_date = date('Y-01-01');
        $end_date = date('Y-12-31');
        $cond = [
            'tenant_id' => $request->input('tenant_id'),
            'company_id' => $request->input('company_id')
        ];
        if($request->input('branch_id')){
            $cond['branch_id'] = $request->input('branch_id');
        }
        if($request->input('emp_no')){
            $cond['emp_no'] = $request->input('emp_no');
        }
        if($request->input('type')){
            $cond['type'] = $request->input('type');
        }
        if($request->input('start_date')){
            $start_date  = $request->input('start_date');
        }
        if($request->input('end_date')){
            $end_date  = $request->input('end_date');
        }
        if($request->input('limit')){
            $limit = $request->input('limit');
        }

        $result = AttendanceRecordsModel::where($cond)->whereBetween('attendance_date',[$start_date,$end_date])->orderBy('id','desc')->paginate($limit);

        if($result){
            return  $result ;
        }
        return response([
            'status' => 0,
            'message' => 'Error processing'
        ]);
    }

    public function getMonthyReportByTenant(Request $request){
        $start_date = date('Y-m-01');
        $end_date = date('Y-m-31');

        $cond = [
            'tenant_id' => $request->input('tenant_id'),
            'company_id' => $request->input('company_id')
        ];


        if($request->input('emp_no')){
            $cond['emp_no'] = $request->input('emp_no');
        }

        if($request->input('start_date')){
            $start_date  = $request->input('start_date');
        }
        if($request->input('end_date')){
            $end_date  = $request->input('end_date');
        }


        $result = AttendanceRecordsModel::select(
            DB::raw('DATE(created_at) as date'),
            DB::raw('min(attendance_date) as checkIn'),
            DB::raw('max(attendance_date) as checkOut'),
            'emp_no',
            'emp_name_en',
            'emp_name_ar'
            )
        ->where($cond)
        ->whereBetween('attendance_date',[$start_date,$end_date])
        ->groupBy('emp_no','date','emp_name_ar','emp_name_en')
        ->get();


        if($result){
            return response([
                'status' => 0,
                'message' => 'Sucess',
                'data' => $result
            ]);
        }

        return response([
            'status' => 0,
            'message' => 'Error processing',
            'data' => $result
        ]);
    }

}
