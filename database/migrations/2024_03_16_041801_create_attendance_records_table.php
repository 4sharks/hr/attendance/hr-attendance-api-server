<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('hr_attendance_records', function (Blueprint $table) {
            $table->id();
            $table->string('emp_no')->nullable();
            $table->string('emp_name_ar')->nullable();
            $table->string('emp_name_en')->nullable();
            $table->enum('type',['checkIn','checkOut','BreakOut','BreakIn','overtimeIn','overtimeOut']);
            $table->string('location_id')->nullable();
            $table->string('location_name')->nullable();
            $table->string('lat')->nullable();
            $table->string('lng')->nullable();
            $table->string('provider_type')->nullable();
            $table->string('provider_device')->nullable();
            $table->dateTime('attendance_date')->nullable();
            $table->string('tenant_id');
            $table->string('company_id');
            $table->string('branch_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('hr_attendance_records');
    }
};
